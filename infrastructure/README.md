# Static website hosted with S3

For the static website hosted with S3 we need the following components to be
provisioned in this order:

- Route 53 managed domain: allyourba.se
- ACM Certificate for `*.{domain}`
- S3 Bucket for the domain we want to have: test.allyourba.se
  - Require a bucket name to be defined beforehand
- CloudFront distribution for test.allyourba.se
  - Require desired domain defined in advance
  - Require `domain_name` from S3 website
  - Require a unique origin id based on the domain name we want to have
  - Require the ACM ARN
- Route 53 entry for the cloudfront distribution

## TODO

- ~~DynamoDB lock table~~
- ~~S3 state bucket~~
- S3 log bucket
- IAM Maintenance User
- Terraform workspaces
