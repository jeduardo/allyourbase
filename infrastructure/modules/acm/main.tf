
resource "aws_acm_certificate" "cert" {
  domain_name  = "${var.domain_name}"
  subject_alternative_names = "${var.subject_alternative_names}"
  validation_method = "${var.validation_method}"
    
  tags = {
    Environment = "${var.environment}"
    Name = "${var.name}"
    Terraform = "true"
  }

  lifecycle {
    create_before_destroy = true
  }
}
