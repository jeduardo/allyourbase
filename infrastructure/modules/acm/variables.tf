variable "environment" {}
variable "domain_name" {}
variable "validation_method" {}
variable "subject_alternative_names" {
  type = "list"
  default = []
}
variable "name" {
  default = "Managed by Terraform"
}
