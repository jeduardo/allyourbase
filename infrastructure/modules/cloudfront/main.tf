provider "aws" {
  version = "~> 2.0"
  # Cheapest
  region = "us-east-1"
}

resource "aws_cloudfront_distribution" "cloudfront" {
  # Domain names the distribution will respond to
  # https://aws.amazon.com/premiumsupport/knowledge-center/resolve-cloudfront-bad-request-error/
  aliases = ["${var.id}"]

  origin {
    # For S3 buckets hosting websites you need to use the website endpoint
    domain_name = "${var.domain_name}"
    # Unique origin ID for this distribution
    origin_id   = "${var.id}"

    # Anything else than a REST call is a custom origin
    # https://stackoverflow.com/questions/55040555/terraform-cant-create-a-cloudfronts-origin-with-a-static-s3-website-endpoint
    custom_origin_config {
      http_port              = "80"
      https_port             = "443"
      # Websites are served from HTTP only
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }
  }
  
  enabled             = true
  is_ipv6_enabled     = true
  comment             = "${var.comment}"
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    # Same as before
    target_origin_id = "${var.id}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }
  
  # We want only US and EU
  price_class = "${var.price_class}"

  tags = {
    Environment = "${var.environment}"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
      # Hardcoded ARN from the ACM we created
      acm_certificate_arn = "${var.acm_certificate_arn}"
      # We don't want CloudFront to add a virtual IP in front of it, just SNI is fine.
      ssl_support_method = "sni-only"
  }
}
