output "id" {
  value = "${aws_cloudfront_distribution.cloudfront.id}"
}
output "arn" {
  value = "${aws_cloudfront_distribution.cloudfront.arn}"
}

output "caller_reference" {
  value = "${aws_cloudfront_distribution.cloudfront.caller_reference}"
}

output "status"{
  value = "${aws_cloudfront_distribution.cloudfront.status}"
}

output "active_trusted_signers" {
  value = "${aws_cloudfront_distribution.cloudfront.active_trusted_signers}"
}

output "domain_name" {
  value = "${aws_cloudfront_distribution.cloudfront.domain_name}"
}

output "last_modified_time" {
  value = "${aws_cloudfront_distribution.cloudfront.last_modified_time}"
}

output "in_progress_validation_batches" {
  value = "${aws_cloudfront_distribution.cloudfront.in_progress_validation_batches}"
}

output "etag" {
  value = "${aws_cloudfront_distribution.cloudfront.etag}"
}

output "hosted_zone_id" {
  value = "${aws_cloudfront_distribution.cloudfront.hosted_zone_id}"
}
