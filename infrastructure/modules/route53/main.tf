provider "aws" {
  version = "~> 2.0"
  # Cheapest
  region = "us-east-1"
}

resource "aws_route53_zone" "primary" {
  name = "allyourba.se"
  tags = {
    "Environment" = "prod"
  }
}

# Email setup - required to create certificates with ACM
resource "aws_route53_record" "mx" {
  zone_id = "${aws_route53_zone.primary.zone_id}"
  name    = ""
  type    = "MX"
  ttl     = "10800"
  records = ["10 spool.mail.gandi.net.", "50 fb.mail.gandi.net."]
}
resource "aws_route53_record" "spf" {
  zone_id = "${aws_route53_zone.primary.zone_id}"
  name    = ""
  type    = "TXT"
  ttl     = "10800"
  records = ["v=spf1 include:_mailcust.gandi.net ?all"]
}

# S3 bucket name - hardcoded for now
resource "aws_route53_record" "test" {
  zone_id = "${aws_route53_zone.primary.zone_id}"
  name    = "test.allyourba.se"
  type    = "CNAME"
  ttl     = "300"
  # Hardcoded for now
  #records = ["test.allyourba.se.s3-website-us-east-1.amazonaws.com"]
  # Hardcoded cloudfront distribution name
  records = ["dw9du9v0nslki.cloudfront.net"]
}
