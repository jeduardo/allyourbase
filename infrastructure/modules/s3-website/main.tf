resource "aws_s3_bucket" "bucket" {
  bucket = "${var.domain}"
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "error.html"

    routing_rules = "${var.routing_rules}"
  }

  policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [{
    "Sid": "AllowPublicRead",
    "Effect": "Allow",
    "Principal": {"AWS": "*"},
    "Action": ["s3:GetObject"],
    "Resource": ["arn:aws:s3:::${var.domain}/*"]
  }]
}
  EOF

  tags = {
    Environment = "${var.environment}"
    Terraform = "true"
  }
}
