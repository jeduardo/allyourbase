resource "aws_s3_bucket" "bucket" {
  count  = "${length(var.names)}"
  bucket = "${element(var.names, count.index)}"
  acl    = "private"

  tags = {
    Environment = "${var.environment}"
    Terraform = "true"
  }
}

resource "aws_s3_bucket_public_access_block" "bucket" {
  count  = "${length(var.names)}"
  bucket = "${element(aws_s3_bucket.bucket.*.id, count.index)}"

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
