provider "aws" {
  version = "~> 2.0"
  # Cheapest
  region = "us-east-1"
}


terraform {
  # backend "local" {
  #   path = "terraform.tfstate"
  # }
  backend "s3" {
    bucket = "state.allyourba.se"
    key    = "prod/acm/terraform.tfstate"
    dynamodb_table = "state.allyourba.se-prod-lock"
    region = "us-east-1"
  }
}
module "prod-acm" {
  source = "../../modules/acm"
  environment = "prod"
  validation_method = "EMAIL"
  domain_name = "*.allyourba.se"
  subject_alternative_names = ["allyourba.se"]
  name = "wildcard certificate for all allyourba.se websites"
}
