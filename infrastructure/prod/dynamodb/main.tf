provider "aws" {
  version = "~> 2.0"
  # Cheapest
  region = "us-east-1"
}

terraform {
  # backend "local" {
  #   path = "terraform.tfstate"
  # }
  backend "s3" {
    bucket = "state.allyourba.se"
    key    = "prod/dynamodb/terraform.tfstate"
    dynamodb_table = "state.allyourba.se-prod-lock"
    region = "us-east-1"
  }
}

resource "aws_dynamodb_table" "dynamodb-test-lock" {
  name           = "state.allyourba.se-test-lock"
  hash_key       = "LockID"
  read_capacity  = 1
  write_capacity = 1

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name        = "Terraform Lock Table for state.allyourba.se test"
    Environment = "prod"
  }
}

resource "aws_dynamodb_table" "dynamodb-prod-lock" {
  name           = "state.allyourba.se-prod-lock"
  hash_key       = "LockID"
  read_capacity  = 1
  write_capacity = 1

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name        = "Terraform Lock Table for state.allyourba.se prod"
    Environment = "prod"
  }
}
