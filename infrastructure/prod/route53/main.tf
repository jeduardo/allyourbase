provider "aws" {
  version = "~> 2.0"
  # Cheapest
  region = "us-east-1"
}

terraform {
  # backend "local" {
  #   path = "terraform.tfstate"
  # }
  backend "s3" {
    bucket = "state.allyourba.se"
    key    = "prod/route53/terraform.tfstate"
    dynamodb_table = "state.allyourba.se-prod-lock"
    region = "us-east-1"
  }
}

resource "aws_route53_zone" "primary" {
  name = "allyourba.se."
  tags = {
    "Environment" = "prod"
  }
}

# Email setup - required to create certificates with ACM
resource "aws_route53_record" "mx" {
  zone_id = "${aws_route53_zone.primary.zone_id}"
  name    = ""
  type    = "MX"
  ttl     = "10800"
  records = ["10 spool.mail.gandi.net.", "50 fb.mail.gandi.net."]
}
resource "aws_route53_record" "spf" {
  zone_id = "${aws_route53_zone.primary.zone_id}"
  name    = ""
  type    = "TXT"
  ttl     = "10800"
  records = ["v=spf1 include:_mailcust.gandi.net ?all"]
}
