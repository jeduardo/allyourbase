provider "aws" {
  version = "~> 2.0"
  # Cheapest
  region = "us-east-1"
}

terraform {
  # backend "local" {
  #   path = "terraform.tfstate"
  # }
  backend "s3" {
    bucket = "state.allyourba.se"
    key    = "prod/s3/terraform.tfstate"
    dynamodb_table = "state.allyourba.se-prod-lock"
    region = "us-east-1"
  }
}

module "s3" {
  source = "../../modules/s3"
  names = ["state.allyourba.se", "logs.allyourba.se"]
  environment = "prod"
}
