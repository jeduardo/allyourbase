output "id" {
  value = "${module.s3.id}"
}
output "arn" {
  value = "${module.s3.arn}"
}
output "bucket_domain_name" {
  value = "${module.s3.bucket_domain_name}"
}
output "bucket_regional_domain_name" {
  value = "${module.s3.bucket_regional_domain_name}"
}
output "hosted_zone_id" {
  value = "${module.s3.hosted_zone_id}"
}
output "region" {
  value = "${module.s3.region}"
}
