provider "aws" {
  version = "~> 2.0"
  # Cheapest
  region = "us-east-1"
}

terraform {
  # backend "local" {
  #   path = "terraform.tfstate"
  # }
  backend "s3" {
    bucket = "state.allyourba.se"
    key    = "test/cloudfront/terraform.tfstate"
    dynamodb_table = "state.allyourba.se-test-lock"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "s3-website" {
  # backend = "local"

  # config = {
  #   path = "../s3/terraform.tfstate"
  # }
  backend = "s3"
  config = {
    bucket = "state.allyourba.se"
    key    = "test/s3/terraform.tfstate"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "acm" {
  # backend = "local"

  # config = {
  #   path = "../s3/terraform.tfstate"
  # }
  backend = "s3"
  config = {
    bucket = "state.allyourba.se"
    key    = "prod/acm/terraform.tfstate"
    region = "us-east-1"
  }
}

# Sometimes it takes 25 minutes to create/modify a distribution...
module "cloudfront-test" {
  source = "../../modules/cloudfront"
  environment = "test"
  # Terraform 0.12 requires the 'outputs' object, not the root outputs
  id = "${data.terraform_remote_state.s3-website.outputs.id}"
  domain_name = "${data.terraform_remote_state.s3-website.outputs.website_endpoint}"
  price_class = "PriceClass_100"
  acm_certificate_arn = "${data.terraform_remote_state.acm.outputs.arn}"
}