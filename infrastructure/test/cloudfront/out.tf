output "id" {
  value = "${module.cloudfront-test.id}"
}
output "arn" {
  value = "${module.cloudfront-test.arn}"
}

output "caller_reference" {
  value = "${module.cloudfront-test.caller_reference}"
}

output "status"{
  value = "${module.cloudfront-test.status}"
}

output "active_trusted_signers" {
  value = "${module.cloudfront-test.active_trusted_signers}"
}

output "domain_name" {
  value = "${module.cloudfront-test.domain_name}"
}

output "last_modified_time" {
  value = "${module.cloudfront-test.last_modified_time}"
}

output "in_progress_validation_batches" {
  value = "${module.cloudfront-test.in_progress_validation_batches}"
}

output "etag" {
  value = "${module.cloudfront-test.etag}"
}

output "hosted_zone_id" {
  value = "${module.cloudfront-test.hosted_zone_id}"
}
