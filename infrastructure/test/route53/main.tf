provider "aws" {
  version = "~> 2.0"
  # Cheapest
  region = "us-east-1"
}

terraform {
  # backend "local" {
  #   path = "terraform.tfstate"
  # }
  backend "s3" {
    bucket = "state.allyourba.se"
    key    = "test/route53/terraform.tfstate"
    dynamodb_table = "state.allyourba.se-test-lock"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "s3-website" {
  # backend = "local"

  # config = {
  #   path = "../s3/terraform.tfstate"
  # }
  backend = "s3"
  config = {
    bucket = "state.allyourba.se"
    key    = "test/s3/terraform.tfstate"
    region = "us-east-1"
  }
}
data "terraform_remote_state" "cloudfront" {
  # backend = "local"

  # config = {
  #   path = "../s3/terraform.tfstate"
  # }
  backend = "s3"
  config = {
    bucket = "state.allyourba.se"
    key    = "test/cloudfront/terraform.tfstate"
    region = "us-east-1"
  }
}

data "aws_route53_zone" "primary" {
  name = "allyourba.se."
}

resource "aws_route53_record" "test" {
  zone_id = "${data.aws_route53_zone.primary.zone_id}"
  name = "${data.terraform_remote_state.s3-website.outputs.id}"
  type = "A"
  alias {
    name = "${data.terraform_remote_state.cloudfront.outputs.domain_name}"
    zone_id = "${data.terraform_remote_state.cloudfront.outputs.hosted_zone_id}"
    evaluate_target_health = false
  }
}
