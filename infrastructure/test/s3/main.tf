provider "aws" {
  version = "~> 2.0"
  # Cheapest
  region = "us-east-1"
}

terraform {
  # backend "local" {
  #   path = "terraform.tfstate"
  # }
  backend "s3" {
    bucket = "state.allyourba.se"
    dynamodb_table = "state.allyourba.se-test-lock"
    key    = "test/s3/terraform.tfstate"
    region = "us-east-1"
  }
}

module "s3-website-test" {
  source = "../../modules/s3-website"
  domain = "test.allyourba.se"
  environment = "test"
  routing_rules =  <<EOF
[{
    "Condition": {
        "KeyPrefixEquals": "docs/"
    },
    "Redirect": {
        "ReplaceKeyPrefixWith": "documents/"
    }
}]
EOF
}

# Mixing some content upload in the state just for kicks.
resource "aws_s3_bucket_object" "index" {
  key = "index.html"
  bucket = "${module.s3-website-test.id}"
  source = "index.html"
  acl = "public-read"
  content_type = "text/html"
}

resource "aws_s3_bucket_object" "error" {
  key = "error.html"
  bucket = "${module.s3-website-test.id}"
  source = "error.html"
  acl = "public-read"
  content_type = "text/html"
}