output "id" {
  value = "${module.s3-website-test.id}"
}
output "arn" {
  value = "${module.s3-website-test.arn}"
}
output "bucket_domain_name" {
  value = "${module.s3-website-test.bucket_domain_name}"
}
output "bucket_regional_domain_name" {
  value = "${module.s3-website-test.bucket_regional_domain_name}"
}
output "hosted_zone_id" {
  value = "${module.s3-website-test.hosted_zone_id}"
}
output "region" {
  value = "${module.s3-website-test.region}"
}
output "website_endpoint" {
  value = "${module.s3-website-test.website_endpoint}"
}
output "website_domain" {
  value = "${module.s3-website-test.website_domain}"
}
